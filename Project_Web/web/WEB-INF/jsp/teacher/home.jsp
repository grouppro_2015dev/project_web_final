<%-- 
    Document   : add
    Created on : May 16, 2015, 4:56:38 PM
    Author     : thanhnt
--%>
<div class="khung">
    <%@ include file="../layout/header.jsp" %>
    <link href="<c:url value="/resources/styles/css.css"/>" rel="stylesheet" type="text/css" />
 <div class="content_right" >
     <a href="<%=request.getContextPath()%>/api/student/teacher"> API</a>
     <a href="<%=request.getContextPath()%>/teacher/add"> Add new</a>
       <table border="0">
    <tr>
        <th>Name</th>
        <th>Username</th>
        <th>Password</th>
        <th>Class</th>
        
        <c:forEach items="${list_teacher}" var="t" >  
        <tr> 
            <td><c:out value="${t.name}"/></td>
            <td><c:out value="${t.username}"/></td>
            
            <td><c:out value="${t.password}"/></td>  
            <c:forEach items="${t.subjects}" var="s">
                <td><c:out value="${s.idSub}"/></td>
            </c:forEach>
            <td align="center"><a href="<%=request.getContextPath()%>/teacher/edit?code=${t.username}">Edit</a>
            <td align="center"><a href="<%=request.getContextPath()%>/teacher/delete?code=${t.username}">Delete</a>
        </tr>  
    </c:forEach> 
</tr>

</table>
</div>
<%@ include file="../layout/footer.jsp" %>
</div>