<%-- 
    Document   : edit
    Created on : May 18, 2015, 4:02:55 PM
    Author     : thanhnt
--%>

<%-- 
    Document   : add
    Created on : May 13, 2015, 3:06:51 PM
    Author     : thanhnt
--%>

<%@ include file="../layout/header.jsp" %>
<link href="<c:url value="/resources/styles/css.css"/>" rel="stylesheet" type="text/css" />
 <div class="content_right" >
       <div align="center">
    
    <table border="0" width="90%">
        <form:form action="edit" modelAttribute="teacherForm" method="POST" >
            <form:hidden path="username" />
            <tr>
                <td align="left" width="20%">Teacher name: </td>
                <td align="left" width="40%"><form:input path="name" size="30"/></td>
                <td align="left"><form:errors path="name" cssClass="error"/></td>
            </tr>
            <tr>
                <td>Username </td>
                <td><form:input path="username" size="30"/></td>
                <td><form:errors path="username" cssClass="error"/></td>
            </tr>
            <tr>
                <td>Password: </td>
                <td><form:input path="password" size="30"/></td>
                <td><form:errors path="password" cssClass="error"/></td>
            </tr>
           
                    <tr>
                        <td></td>
                        <td align="center"><input type="submit" value="submit"/></td>
                        <td></td>
                    </tr>
                    </form:form>
                </table>
  </div>
 </div>
<%@ include file="../layout/footer.jsp" %>
