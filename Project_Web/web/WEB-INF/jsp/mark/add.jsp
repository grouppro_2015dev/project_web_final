<%-- 
    Document   : add
    Created on : May 16, 2015, 4:56:38 PM
    Author     : thanhnt
--%>

<div class="khung">
    <%@ include file="../layout/header.jsp" %>
    <link href="<c:url value="/resources/styles/css.css"/>" rel="stylesheet" type="text/css" />
<div class="content_right" >
       <div align="center">
    
    <table border="0" width="90%">
        <form:form action="add" modelAttribute="markForm" method="POST" >
            <form:hidden path="student.id" />
             <form:hidden path="subject.idSub" />
            <tr>
                <td align="left" width="20%">Student name: </td>
                <td align="left" width="40%"><form:input path="student.name" size="30"/></td>
                <td align="left"><form:errors path="student.name" cssClass="error"/></td>
            </tr>
            <tr>
                <td>Subject: </td>
                <td><form:input path="subject.nameSub" size="30"/></td>
                <td><form:errors path="subject.nameSub" cssClass="error"/></td>
            </tr>
           <tr>
                <td>15': </td>
                <td><form:input path="mark1" size="30"/></td>
                <td><form:errors path="mark1" cssClass="error"/></td>
            </tr>
            <tr>
                <td>1h: </td>
                <td><form:input path="mark2" size="30"/></td>
                <td><form:errors path="mark2" cssClass="error"/></td>
            </tr>
            <tr>
                <td>Final: </td>
                <td><form:input path="markSum" size="30"/></td>
                <td><form:errors path="markSum" cssClass="error"/></td>
            </tr>
            <tr>
                <td>Comment: </td>
                <td><form:input path="comment" size="30"/></td>
                <td><form:errors path="comment" cssClass="error"/></td>
            </tr>
                    <tr>
                        <td></td>
                        <td align="center"><input type="submit" value="submit"/></td>
                        <td></td>
                    </tr>
                    </form:form>
                </table>
  </div>
 </div>
<%@ include file="../layout/footer.jsp" %>
</div>