<%-- 
    Document   : home
    Created on : May 13, 2015, 11:15:26 AM
    Author     : thanhnt
--%>



<div class="khung">
    <%@ include file="../layout/header.jsp" %>
    <link href="<c:url value="/resources/styles/css.css"/>" rel="stylesheet" type="text/css" />
 <div class="content_right" >
     <table  style="margin-left: 10px" style="border-width: thick">
        <tr  style="border-width: thick">
        <th>ID</th>
        <th>Student name</th>
        <th>Age</th>
        <th>Address</th>
        <th>Contact</th>
        <th>Action</th>

        <c:forEach items="${list_student}" var="student">  
        <tr>  
            <td><c:out value="${student.id}"/></td>  
            <td><c:out value="${student.name}"/></td>  
            <td><c:out value="${student.age}"/></td>  
            <td><c:out value="${student.address}"/></td>
            <td><c:out value="${student.contact}"/></td>
            
            <td><a href="<%=request.getContextPath()%>/employee?departmentId=${student.id}"></a></td>  
            <td align="center"><a href="<%=request.getContextPath()%>/student/edit?id=${student.id}">Edit</a> |
            <a href="<%=request.getContextPath()%>/student/delete?id=${student.id}">Delete</a></td>  
        </tr>  
    </c:forEach> 
    </tr>

</table>
 </div>
      
<%@ include file="../layout/footer.jsp" %>s
</div>
 