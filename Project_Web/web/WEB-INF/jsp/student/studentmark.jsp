<%-- 
    Document   : home
    Created on : May 13, 2015, 11:15:26 AM
    Author     : thanhnt
--%>

<div class="khung">
    <%@ include file="../layout/header.jsp" %>
    <link href="<c:url value="/resources/styles/css.css"/>" rel="stylesheet" type="text/css" />
 <div class="content_right" >
    <table border="1" style="margin-left: 20px; margin-top: 20px">
    <tr>
        <th>ID</th>
        <th>Student name</th>
        <th>Age</th>
        <th>English</th>
        <th>Java</th>
        <th>Plt</th>
        <th>Web</th>
        

        <c:forEach items="${list_student}" var="student">  
        <tr>  
            
            <td><c:out value="${student.getId()}"/></td>  
            <td><c:out value="${student.name}"/></td>  
            <td><c:out value="${student.age}"/></td>  
<!--            <td><c:out value="${student.marks}"/></td>  -->
            <c:forEach items="${student.marks}" var="mark">
                <td><c:out value="${mark.mark1}"/></td>  
            </c:forEach>
        </tr>  
    </c:forEach> 
</tr>

</table>
 </div>
<%@ include file="../layout/footer.jsp" %>
</div>