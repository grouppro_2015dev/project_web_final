<%-- 
    Document   : add
    Created on : May 13, 2015, 3:06:51 PM
    Author     : thanhnt
--%>

<%@ include file="../layout/header.jsp" %>
<link href="<c:url value="/resources/styles/css.css"/>" rel="stylesheet" type="text/css" />
 <div class="content_right" >
       <div align="center">
    
    <table border="0" width="90%">
        <form:form action="add" modelAttribute="studentForm" method="POST" >
            <form:hidden path="id" />
            <tr>
                <td align="left" width="20%">Student name: </td>
                <td align="left" width="40%"><form:input path="name" size="30"/></td>
                <td align="left"><form:errors path="name" cssClass="error"/></td>
            </tr>
            <tr>
                <td>Student age: </td>
                <td><form:input path="age" size="30"/></td>
                <td><form:errors path="age" cssClass="error"/></td>
            </tr>
           <tr>
                <td>Student address: </td>
                <td><form:input path="address" size="30"/></td>
                <td><form:errors path="address" cssClass="error"/></td>
            </tr>
            <tr>
                <td>Student contact: </td>
                <td><form:input path="contact" size="30"/></td>
                <td><form:errors path="contact" cssClass="error"/></td>
            </tr>
                    <tr>
                        <td><a href="<%=request.getContextPath()%>/student/" class="button">Back</a></td>
                        <td align="center"><input type="submit" value="submit"/></td>
                        <td></td>
                    </tr>
                    </form:form>
                </table>
  </div>
 </div>
<%@ include file="../layout/footer.jsp" %>
