
import com.pnv.models.Student;
import com.pnv.utils.NewHibernateUtil;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Session;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author thanhnt
 */
public class testconnect {
    public static void main(String[] args) {
        Session session =  NewHibernateUtil.getSessionFactory().openSession();
        
        List<Student> students = session.createQuery("from Student ").list();
        for (Iterator<Student> iterator = students.iterator(); iterator.hasNext();) {
            Student next = iterator.next();
            System.out.println(next.getName());
        }
        
        session.close();
    }
}
