/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.models;

/**
 *
 * @author thanhnt
 */
public class StudentMark {
    private String name;
    private String address;
    private float mark1;
    private float mark2;
    private float marksum;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public float getMark1() {
        return mark1;
    }

    public void setMark1(float mark1) {
        this.mark1 = mark1;
    }

    public float getMark2() {
        return mark2;
    }

    public void setMark2(float mark2) {
        this.mark2 = mark2;
    }

    public float getMarksum() {
        return marksum;
    }

    public void setMarksum(float marksum) {
        this.marksum = marksum;
    }
    
    
    
}
