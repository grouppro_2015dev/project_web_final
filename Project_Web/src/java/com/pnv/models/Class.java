package com.pnv.models;
// Generated May 12, 2015 9:39:45 AM by Hibernate Tools 3.2.1.GA


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Class generated by hbm2java
 */
@Entity
@Table(name="class"
    ,catalog="project"
)
public class Class  implements java.io.Serializable {


     private Integer idClass;
     private Teacher teacher;
     private Student student;

    public Class() {
    }

    public Class(Teacher teacher, Student student) {
       this.teacher = teacher;
       this.student = student;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)
    
    @Column(name="ID_Class", unique=true, nullable=false)
    public Integer getIdClass() {
        return this.idClass;
    }
    
    public void setIdClass(Integer idClass) {
        this.idClass = idClass;
    }
@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_Teacher", nullable=false)
    public Teacher getTeacher() {
        return this.teacher;
    }
    
    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }
@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="ID_Stu", nullable=false)
    public Student getStudent() {
        return this.student;
    }
    
    public void setStudent(Student student) {
        this.student = student;
    }




}


