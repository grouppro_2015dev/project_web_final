/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controller;

import com.pnv.dao.MarkDao;
import com.pnv.dao.StudentDao;
import com.pnv.dao.SubjectDao;
import com.pnv.models.Mark;
import com.pnv.models.Student;
import com.pnv.utils.NewHibernateUtil;
import java.util.List;
import javax.validation.Valid;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author thanhnt
 */
@Controller
@RequestMapping(value = "/mark")
public class MarkController {
    
    private final SessionFactory sessionFactory = NewHibernateUtil.getSessionFactory();
    @Autowired
    private StudentDao studentDao;
    @Autowired
    private SubjectDao subjectDao;
    @Autowired
    private MarkDao markDao;
    
    // Tra ve trang home
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String viewStudentSubject(ModelMap map){
        List<Mark> list_mark = markDao.findAll();
        map.put("list_mark", list_mark);
        return "mark/add";
    }
     //Xem diem theo English
    @RequestMapping(value = "/viewenglish", method = RequestMethod.GET)
    public String viewStudentSubject1(ModelMap map){
        List<Mark> list_mark = markDao.findAllbySub("English");
        map.put("thanh", list_mark);
        return "mark/view";
    }
//    // Sua diem EngLish
//    @RequestMapping(value = "/edit", method = RequestMethod.GET)
//    public String viewEditTitlePage(@RequestParam(value = "id", required = true) int id, ModelMap map) {
//
//        Mark markForm = markDao.findAllbyStu("English",id);
//        map.addAttribute("markForm", markForm);
//        return "mark/add";
//    }
//    // xu ly edit
//    @RequestMapping(value = "/add", method = RequestMethod.POST)
//    public String doAddNew(@Valid @ModelAttribute("markForm") Mark markForm,
//            BindingResult result, ModelMap map) {
//
//        if (result.hasErrors()) {
//            map.addAttribute("markForm", markForm);
//            return "mark/add";
//        }
//        markDao.save(markForm);
//
//        /**
//         * Get all titles
//         */
//        List<Mark> list_mark = markDao.findAllbySub("English");
//        map.put("thanh", list_mark);
//        return "mark/add";
//    }
    
    
    
   // Java
    
    @RequestMapping(value = "/viewjava", method = RequestMethod.GET)
    public String viewjavaMark(ModelMap map){
        List<Mark> list_mark = markDao.findAllbySub("Java");
        map.put("thanh", list_mark);
        return "mark/view";
    }
    
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String viewEditFava(@RequestParam(value = "id", required = true) int id, ModelMap map) {

        Mark markForm = markDao.findAllbyStu("Java",id);
        map.addAttribute("markForm", markForm);
        return "mark/add";
    }

//        @RequestMapping(value = "/addnew", method = RequestMethod.GET)
//    public String viewAddNewPage(ModelMap map) {
//
//        Mark markForm = new Mark();
//        map.addAttribute("markForm", markForm);
//        return "mark/add";
//
//    }
    
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String doeditJava(@Valid @ModelAttribute("markForm") Mark markForm,
            BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            map.addAttribute("markForm", markForm);
            return "mark/add";
        }
        markDao.update(markForm);

        /**
         * Get all titles
         */
        List<Mark> list_mark = markDao.findAllbySub("Java");
        map.put("thanh", list_mark);
        map.addAttribute("add_success", "ok");

        return "mark/viewjava";
    }
    
    // Theo PLT
        @RequestMapping(value = "/viewplt", method = RequestMethod.GET)
        public String viewPLT(ModelMap map){
        List<Mark> list_mark = markDao.findAllbySub("PLT");
        map.put("thanh", list_mark);
        return "mark/view";
    }
    // Theo Web
        
        @RequestMapping(value = "/viewweb", method = RequestMethod.GET)
        public String viewWeb(ModelMap map){
        List<Mark> list_mark = markDao.findAllbySub("Web");
        map.put("thanh", list_mark);
        return "mark/view";
    }
    
}
