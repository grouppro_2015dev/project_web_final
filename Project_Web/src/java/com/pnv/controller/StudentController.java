/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controller;

import com.pnv.dao.MarkDao;
import com.pnv.dao.StudentDao;
import com.pnv.dao.SubjectDao;
import com.pnv.models.Mark;
import com.pnv.models.Student;
import com.pnv.models.Subject;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author thanhnt
 */
@Controller
@RequestMapping(value = "/student")
public class StudentController {
    
    @Autowired
    private StudentDao studentDao;
    @Autowired
    private SubjectDao subjectDao;
    @Autowired
    private MarkDao markDao;
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String viewStudent(ModelMap map){
        List<Student> list_student = studentDao.findAll();
        map.put("list_student", list_student);
        return "student/home";
    }
    
        @RequestMapping(value = "/subject", method = RequestMethod.GET)
    public String viewStudentSubject(ModelMap map){
        List<Mark> list_mark = markDao.findAll();
        map.put("list_mark", list_mark);
//        map.put("marks", marks);
        return "student/studentsubject";
    }
        
            @RequestMapping(value = "/details", method = RequestMethod.GET)
    public String viewStudentMarkSubject(ModelMap map){
        List<Subject> list_sub = subjectDao.findAll();
        map.put("list_sub", list_sub);
//        map.put("marks", marks);
        return "student/markbyname";
    }
    
    
    @RequestMapping(value = "/mark", method = RequestMethod.GET)
    public String viewStudentMark(ModelMap map){
        List<Student> list_student = studentDao.findAll();

        map.put("list_student", list_student);
//        map.put("marks", marks);
        return "student/studentmark";
    }
    

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String viewEditTitlePage(@RequestParam(value = "id", required = true) int id, ModelMap map) {

        Student studentForm = studentDao.findByStudentId(id);
        map.addAttribute("studentForm", studentForm);
        return "student/add";
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {

        Student studentForm = new Student();
        map.addAttribute("studentForm", studentForm);
        return "student/add";

    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String doAddNew(@Valid @ModelAttribute("studentForm") Student studentForm,
            BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            map.addAttribute("studentForm", studentForm);
            return "student/add";
        }
        studentDao.save(studentForm);

        /**
         * Get all titles
         */
        List<Student> list_student = studentDao.findAll();
        map.put("list_student", list_student);
        map.addAttribute("add_success", "ok");

        return "student/home";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String doDeleteTitle(@RequestParam(value = "id", required = true) int id, ModelMap map) {

        studentDao.delete(studentDao.findByStudentId(id));

        return "student/home";

    }
    
    
}
