/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controller;

import com.pnv.dao.StudentDao;
import com.pnv.dao.TeacherDao;
import com.pnv.models.Student;
import com.pnv.models.Teacher;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author thanhnt
 */
@Controller
@RequestMapping(value = "/teacher")
public class TeacherController {
    
    @Autowired
    private TeacherDao teacherDao;
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String viewTeacher(ModelMap map){
        List<Teacher> list_teacher = teacherDao.findAll();
        map.put("list_teacher", list_teacher);
//        map.put("marks", marks);
        return "teacher/home";
    }
    
     @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String viewEditTitlePage(@RequestParam(value = "code", required = true) String code, ModelMap map) {

        Teacher teacherForm = teacherDao.findByTeacherCode(code);
        map.addAttribute("teacherForm", teacherForm);
        return "teacher/edit";
    }
     
     @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String doEdit(@Valid @ModelAttribute("teacherForm") Teacher teacherForm,
            BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            map.addAttribute("teacherForm", teacherForm);
            return "teacher/edit";
        }
        teacherDao.update(teacherForm);

        /**
         * Get all titles
         */
        List<Teacher> list_teacher = teacherDao.findAll();
        map.put("list_teacher", list_teacher);
        map.addAttribute("add_success", "ok");

        return "teacher/home";
    }
    
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String viewAddNewPage(ModelMap map) {

        Teacher teacherForm = new Teacher();
        map.addAttribute("teacherForm", teacherForm);
        return "teacher/add";

    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String doAddNew(@Valid @ModelAttribute("teacherForm") Teacher teacherForm,
            BindingResult result, ModelMap map) {

        if (result.hasErrors()) {
            map.addAttribute("teacherForm", teacherForm);
            return "teacher/add";
        }
        teacherDao.save(teacherForm);

        /**
         * Get all titles
         */
        List<Teacher> list_teacher = teacherDao.findAll();
        map.put("list_teacher", list_teacher);
        map.addAttribute("add_success", "ok");

        return "teacher/home";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String doDeleteTitle(@RequestParam(value = "code", required = true) String code, ModelMap map) {

        teacherDao.delete(teacherDao.findByTeacherCode(code));

        return "teacher/home";

    }
    
}
