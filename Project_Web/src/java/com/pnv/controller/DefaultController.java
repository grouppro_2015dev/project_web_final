/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.controller;

import com.pnv.dao.HomeDao;
import com.pnv.models.Student;
import com.pnv.models.StudentMark;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author thanhnt
 */
@Controller
public class DefaultController {
    
    @Autowired
    private HomeDao homeDao;
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String viewWelcomePage(ModelMap map) {
        return "intro/contact";
    }
    @RequestMapping(value = "/intro", method = RequestMethod.GET)
    public String viewIntro(ModelMap map) {
        return "intro/intro";
    }
      @RequestMapping(value = "/Entertainment", method = RequestMethod.GET)
    public String viewEnter(ModelMap map) {
        return "intro/Entertainment";
    }
 
    
    @RequestMapping(value = "/mark", method = RequestMethod.GET)
    public String viewStudentMark(ModelMap map){
        List<StudentMark> list_student_mark = homeDao.findMarkbySubject2();
        map.put("list_student_mark", list_student_mark);
        return "mark";
    }
    
}
