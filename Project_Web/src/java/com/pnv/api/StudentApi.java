/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.api;

import com.pnv.dao.StudentDao;
import com.pnv.models.Student;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author thanhnt
 */
@Controller
@RequestMapping(value = "api/student")
public class StudentApi {
    @Autowired
    private StudentDao studentDao;
    @RequestMapping(value = "/",method = RequestMethod.GET)
    public  @ResponseBody List<Student> viewDepartmentPage() {
        /**
         * Get all titles
         */
        List<Student> students_list = studentDao.findAll();
        return students_list;
    }
    
    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public  @ResponseBody Student getDepartmentByID(@PathVariable(value = "id") Integer id) {
        
        Student stu = studentDao.findByStudentId(id);
       // dep.setEmployees(emp_lst);
        return studentDao.findByStudentId(id);
    }
    
}
