/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.api;

import com.pnv.dao.MarkDao;
import com.pnv.dao.StudentDao;
import com.pnv.dao.TeacherDao;
import com.pnv.models.Mark;
import com.pnv.models.Student;
import com.pnv.models.Teacher;
import com.pnv.utils.NewHibernateUtil;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author thanhnt
 */

@Controller
@RequestMapping(value = "api/student")
public class MarkApi {
    private final SessionFactory sessionFactory = NewHibernateUtil.getSessionFactory();
    
    
    @Autowired
    private MarkDao markDaoo;
    @Autowired
    private TeacherDao teacherDao;
    
    @RequestMapping(value = "/teacher",method = RequestMethod.GET)
    public  @ResponseBody List<Object[]> apiteacher(){
        String sql = "";
        Session session = sessionFactory.openSession();
        sql = "select teacher.name, teacher.username, teacher.password, subject.id_sub from teacher, subject where teacher.username = subject.id_teacher";
        List<Object[]> mark_list  = session.createSQLQuery(sql).list();
        return mark_list;
        
    }
    
    @RequestMapping(value = "/api",method = RequestMethod.GET)
    public  @ResponseBody List<Object[]> viewDepartmentPage(@RequestParam(value = "code", required = true) String code) {
        /**
         * Get all titles
         */
        String sql = "";
        Session session = sessionFactory.openSession();
        
        if(code.equals("Java")){
            sql = "select student.name, mark.mark1, mark.mark2, mark.mark_sum, mark.comment from student, mark where student.id = mark.id_stu and mark.id_sub = 'Java'";
        }
        else if(code.equals("Web")){
            sql = "select student.name, mark.mark1, mark.mark2, mark.mark_sum, mark.comment from student, mark where student.id = mark.id_stu and mark.id_sub = 'Web'";
        }
        else if(code.equals("English")){
            sql = "select student.name, mark.mark1, mark.mark2, mark.mark_sum, mark.comment from student, mark where student.id = mark.id_stu and mark.id_sub = 'English'";
        }
        else{
            sql = "select student.name, mark.mark1, mark.mark2, mark.mark_sum, mark.comment from student, mark where student.id = mark.id_stu and mark.id_sub = 'PLT'";
        }
        List<Object[]> mark_list  = session.createSQLQuery(sql).list();
        return mark_list;
    }
}
