/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.StudentMark;
import java.util.List;

/**
 *
 * @author thanhnt
 */
public interface HomeDao {
    
    public List<Object[]> findMarkbySubject();
    
    public List<StudentMark> findMarkbySubject2();
    
    public List<StudentMark[]> findMarkbySubject3();
    
}
