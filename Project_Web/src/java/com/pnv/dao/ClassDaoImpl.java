/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Class;
import com.pnv.utils.NewHibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;

/**
 *
 * @author thanhnt
 */
@Service
public class ClassDaoImpl implements ClassDao{

    private final SessionFactory sessionFactory = NewHibernateUtil.getSessionFactory();
    
    @Override
    public void save(Class class1) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try{
            session.save(class1);
            transaction.commit();
        } catch(Exception e){
            transaction.rollback();
            System.err.println("ERRO:"+e);
        }
        finally{
            session.close();
        }
    }

    @Override
    public void update(Class class1) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try{
            session.update(class1);
            transaction.commit();
        } catch(Exception e){
            transaction.rollback();
            System.err.println("ERRO:"+e);
        }
        finally{
            session.close();
        }
    }

    @Override
    public void delete(Class class1) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try{
            session.delete(class1);
            transaction.commit();
        } catch(Exception e){
            transaction.rollback();
            System.err.println("ERRO:"+e);
        }
        finally{
            session.close();
        }
    }

    @Override
    public List<Class> findAll() {
        Session session = sessionFactory.openSession();
        List<Class> list_Classes = session.createQuery("from Class").list();
        return list_Classes;
    }

    @Override
    public Class findByClassId(int id) {
        Class class1 = null;
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            class1 = (Class) session.get(Class.class, id);
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        return class1;
    }
    
}
