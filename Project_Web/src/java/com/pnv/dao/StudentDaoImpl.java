/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Student;
import com.pnv.utils.NewHibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;

/**
 *
 * @author thanhnt
 */

@Service
public class StudentDaoImpl implements StudentDao{

    private final SessionFactory sessionFactory = NewHibernateUtil.getSessionFactory();
    
    @Override
    public void save(Student student) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try{
            session.saveOrUpdate(student);
            transaction.commit();
        } catch(Exception e){
            transaction.rollback();
            System.err.println("ERRO:"+e);
        }
        finally{
            session.close();
        }
    }

    @Override
    public void update(Student student) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try{
            session.update(student);
            transaction.commit();
        } catch(Exception e){
            transaction.rollback();
            System.err.println("ERRO:"+e);
        }
        finally{
            session.close();
        }
    }

    @Override
    public void delete(Student student) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try{
            session.delete(student);
            transaction.commit();
        } catch(Exception e){
            transaction.rollback();
            System.err.println("ERRO:"+e);
        }
        finally{
            session.close();
        }
    }

    @Override
    public List<Student> findAll() {
        Session session = sessionFactory.openSession();
        List<Student> list_Students = session.createQuery("from Student").list();
        
        return list_Students;
    }

    @Override
    public Student findByStudentId(int id) {
        Student student = null;
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            student = (Student) session.get(Student.class, id);
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        return student;
    }
    
}
