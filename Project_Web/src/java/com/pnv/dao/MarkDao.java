/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Mark;
import java.util.List;

/**
 *
 * @author thanhnt
 */
public interface MarkDao {
    
    public void save(Mark mark);
    
    public void update(Mark mark);
    
    public void delete(Mark mark);
    
    public List<Mark> findAll();
    
    public List<Mark> findAllbySub(String code);
    
    public Mark findAllbyStu(String code,int id);
    
    public List<Mark> findAllbySub2(String code);
    
    public Mark findByMarkId(int id);
}
