/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Mark;
import com.pnv.models.Student;
import com.pnv.utils.NewHibernateUtil;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author thanhnt
 */
@Service
public class MarkDaoImpl implements MarkDao{

    private final SessionFactory sessionFactory = NewHibernateUtil.getSessionFactory();
    
    
    @Override
    public void save(Mark mark) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try{
            session.save(mark);
            transaction.commit();
        } catch(Exception e){
            transaction.rollback();
            System.err.println("ERRO:"+e);
        }
        finally{
            session.close();
        }
    }

    @Override
    public void update(Mark mark) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try{
            session.update(mark);
            transaction.commit();
        } catch(Exception e){
            transaction.rollback();
            System.err.println("ERRO:"+e);
        }
        finally{
            session.close();
        }
    }

    @Override
    public void delete(Mark mark) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try{
            session.delete(mark);
            transaction.commit();
        } catch(Exception e){
            transaction.rollback();
            System.err.println("ERRO:"+e);
        }
        finally{
            session.close();
        }
    }

    @Override
    public List<Mark> findAll() {
        Session session = sessionFactory.openSession();
        List<Mark> list_Marks = session.createQuery("from Mark where id_sub = 'Java'").list();
        return list_Marks;
    }

    @Override
    public Mark findByMarkId(int id) {
        Mark mark = null;
        Session session = sessionFactory.getCurrentSession();
        Transaction transaction = session.beginTransaction();
        try {
            mark = (Mark) session.get(Mark.class, id);
        } catch (HibernateException hb) {
            transaction.rollback();
            System.err.println("error" + hb);
        } finally {
            session.close();
        }
        return mark;
    }

    @Override
    public List<Mark> findAllbySub(String code) {
//select s.name, sb.name_sub, m.mark1, m.mark2, m.mark_sum from mark m, student s, subject sb where m.id_stu = s.id and m.id_sub = :id_sub
        Session session = sessionFactory.openSession();
        Query q = session.createQuery("from Mark where id_sub =:id_subject");
        q.setParameter("id_subject", code);
        List<Mark> list_Marks = q.list();
        return list_Marks;
    }

    

    @Override
    public List<Mark> findAllbySub2(String code) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Mark findAllbyStu(String code, int id) {
        
        Mark mark = null;
        String strQuery = "from Mark where id_stu =:id_student and id_sub =:id_subject  ";
        Session session = sessionFactory.openSession();
        Query query = session.createQuery(strQuery);
        query.setParameter("id_student", id);
        query.setParameter("id_subject", code);
        mark = (Mark) query.uniqueResult();
        session.close();
        return mark;
        
    }
    
}
