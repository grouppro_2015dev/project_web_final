/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Student;
import java.util.List;

/**
 *
 * @author thanhnt
 */
public interface StudentDao {
    
    public void save(Student student);
    
    public void update(Student student);
    
    public void delete(Student student);
    
    public List<Student> findAll();
    
    public Student findByStudentId(int id);
    
    
}
