/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Subject;
import com.pnv.models.Teacher;
import com.pnv.utils.NewHibernateUtil;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;

/**
 *
 * @author thanhnt
 */
@Service
public class TeacherDaoImpl implements TeacherDao{

    private final SessionFactory sessionFactory = NewHibernateUtil.getSessionFactory();
    
    @Override
    public void save(Teacher teacher) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try{
            session.saveOrUpdate(teacher);
            transaction.commit();
        } catch(Exception e){
            transaction.rollback();
            System.err.println("ERRO:"+e);
        }
        finally{
            session.close();
        }
    }

    @Override
    public void update(Teacher teacher) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try{
            session.update(teacher);
            transaction.commit();
        } catch(Exception e){
            transaction.rollback();
            System.err.println("ERRO:"+e);
        }
        finally{
            session.close();
        }
    }

    @Override
    public void delete(Teacher teacher) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try{
            session.delete(teacher);
            transaction.commit();
        } catch(Exception e){
            transaction.rollback();
            System.err.println("ERRO:"+e);
        }
        finally{
            session.close();
        }
    }

    @Override
    public List<Teacher> findAll() {
        Session session = sessionFactory.openSession();
        List<Teacher> list_Teachers = session.createQuery("from Teacher").list();
        return list_Teachers;
    }

    @Override
    public Teacher findByTeacherCode(String code) {
        Teacher teacher = null;
        String strQuery = "from Teacher WHERE Username = :Username ";
        Session session = sessionFactory.openSession();
        Query query = session.createQuery(strQuery);
        query.setParameter("Username", code);
        teacher = (Teacher) query.uniqueResult();
        session.close();
        return teacher;
    }
    
}
