/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Subject;
import java.util.List;

/**
 *
 * @author thanhnt
 */
public interface SubjectDao {
    
    public void save(Subject subject);
    
    public void update(Subject subject);
    
    public void delete(Subject subject);
    
    public List<Subject> findAll();
    
    public Subject findBySubjectCode(String code);
    
}
