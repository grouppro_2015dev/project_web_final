/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Student;
import com.pnv.models.StudentMark;
import com.pnv.utils.NewHibernateUtil;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;

/**
 *
 * @author thanhnt
 */
@Service
public class HomeDaoImpl implements HomeDao{

    private final SessionFactory sessionFactory = NewHibernateUtil.getSessionFactory();
    
    @Override
    public List<Object[]> findMarkbySubject() {
        
        Session session = sessionFactory.openSession();
        Query q = session.createQuery("select s.name, s.address, m.markSum from Student s, Mark m where s.id = m.student.id");
        List<Object[]> list_Objects= (List<Object[]>)q.list();
//        List<Object[]> list_Objects = session.createQuery("select s.name, s.address from Student s").list();
        for(Object[] objects : list_Objects){
            String name1 = (String)objects[0];
            String address1 = (String)objects[1];
            Float mark1 = (Float)objects[2];
        }
        
        
        
        return list_Objects;
    }

    @Override
    public List<StudentMark> findMarkbySubject2() {
        Session session = sessionFactory.openSession();
        Query q = session.createQuery("select s.name, s.address, m.markSum from Student s, Mark m where s.id = m.student.id");
        List<StudentMark> list_Objects= (List<StudentMark>)q.list();
//        List<Object[]> list_Objects = session.createQuery("select s.name, s.address from Student s").list();
        for(StudentMark objects : list_Objects){
            String name1 = (String)objects.getName();
            String address1 = (String)objects.getAddress();
            Float mark1 = (Float)objects.getMark1();
        }
        return list_Objects;
    }

    @Override
    public List<StudentMark[]> findMarkbySubject3() {
        Session session = sessionFactory.openSession();
        Query q = session.createQuery("select s.name, s.address, m.markSum from Student s, Mark m where s.id = m.student.id");
        List<StudentMark[]> list_Objects= (List<StudentMark[]>)q.list();
//        List<Object[]> list_Objects = session.createQuery("select s.name, s.address from Student s").list();
        for(StudentMark[] objects : list_Objects){
            String name1 = (String)objects[0].toString();
            String address1 = (String)objects[1].toString();
            Float mark1 = Float.parseFloat(objects[2].toString());
        }
        return list_Objects;
    }
    
}
