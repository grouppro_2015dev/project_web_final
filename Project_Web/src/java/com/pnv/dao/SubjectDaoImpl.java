/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Student;
import com.pnv.models.Subject;
import com.pnv.utils.NewHibernateUtil;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;

/**
 *
 * @author thanhnt
 */
@Service
public class SubjectDaoImpl implements SubjectDao{

    private final SessionFactory sessionFactory = NewHibernateUtil.getSessionFactory();
    
    @Override
    public void save(Subject subject) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try{
            session.save(subject);
            transaction.commit();
        } catch(Exception e){
            transaction.rollback();
            System.err.println("ERRO:"+e);
        }
        finally{
            session.close();
        }
    }

    @Override
    public void update(Subject subject) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try{
            session.update(subject);
            transaction.commit();
        } catch(Exception e){
            transaction.rollback();
            System.err.println("ERRO:"+e);
        }
        finally{
            session.close();
        }
    }

    @Override
    public void delete(Subject subject) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        try{
            session.delete(subject);
            transaction.commit();
        } catch(Exception e){
            transaction.rollback();
            System.err.println("ERRO:"+e);
        }
        finally{
            session.close();
        }
    }

    @Override
    public List<Subject> findAll() {
        Session session = sessionFactory.openSession();
        List<Subject> list_Subjects = session.createQuery("from Subject").list();
        return list_Subjects;
    }

    @Override
    public Subject findBySubjectCode(String code) {
        Subject subject = null;
        String strQuery = "from Subject WHERE ID_Sub = :ID_Sub ";
        Session session = sessionFactory.openSession();
        Query query = session.createQuery(strQuery);
        query.setParameter("ID_Sub", code);
        subject = (Subject) query.uniqueResult();
        session.close();
        return subject;
    }
    
}
