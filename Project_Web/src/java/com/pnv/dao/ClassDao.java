/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;
import com.pnv.models.Class;
import java.util.List;
/**
 *
 * @author thanhnt
 */
public interface ClassDao {
    
    public void save(Class class1);
    
    public void update(Class class1);
    
    public void delete(Class class1);
    
    public List<Class> findAll();
    
    public Class findByClassId(int id);
    
}
