/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pnv.dao;

import com.pnv.models.Teacher;
import java.util.List;

/**
 *
 * @author thanhnt
 */
public interface TeacherDao {
    
    public void save(Teacher teacher);
    
    public void update(Teacher teacher);
    
    public void delete(Teacher teacher);
    
    public List<Teacher> findAll();
    
    public Teacher findByTeacherCode(String code);
}
